import React from "react"
import Services from "../../services"

import "./index.css"
class addItem extends React.Component {
    constructor() {
        super()
        this.state = {
            title: "",
            body: "",
        }
    }
    services = new Services()

    postData = (event) => {
        event.preventDefault();
        const dataItem = {
            title: this.state.title,
            body: this.state.body
        }
        this.services
            .addItem(dataItem)
            .then((body) => {
                console.log(body)
            })
        this.setState({  
            title: "",
            body: "",
           })   
    }


    render() {

        return (
            <div className="addItem">
                <form onSubmit = {this.postData}>
                    <input onChange = {(event) => {
                        this.setState({
                            title: event.target.value,
                        });
                    }}
                        type = "text"
                        value = {this.state.title}
                    /><br />
                    <input onChange = {(event) => {
                        this.setState({
                            body: event.target.value,
                        });
                    }}
                        type = "text"
                        value = {this.state.body}
                    /><br />
                    <button>addItem</button>
                </form>
            </div>
        )
    }
}
export default addItem

