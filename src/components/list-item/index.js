import React from "react"
import Services from "../../services"
import Spinner from "../spinner"


class ListItem extends React.Component {
    constructor() {
        super()
        this.state = {
            item: [],
            loading: true
        }
    }

    services = new Services()
    update(){
        this.services
        .getItemId(this.props.match.params.id)
        .then((item) => {
            this.setState({
                item,
                loading: false
            })
        })
    }
    componentDidMount() {
      this.update() 
    }
    componentDidUpdate(prevState){
        if(this.state.id !== prevState.id){
            this.update() 
        }
    }
    
    render() {
        const { loading, item } = this.state
        return (
            <div>
                {loading ?
                <Spinner/> :
                  <ul className="itemsList">
                          <li>    {item.id}    </li>
                          <li>  {item.title}   </li>
                          <li>  {item.body}   </li>
                 </ul> }
            </div>
        )
    }
}

export default ListItem