import React from 'react';
import "./index.css"

export default class Spinner extends React.Component {
  render() {
    return (
      <div>
    <div className="lds-css ng-scope">
        <div className="lds-spinner">
            <div>
                </div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
        </div>
      </div>
    );
  }
}
