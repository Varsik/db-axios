import React from "react"
import Services from "../../services"
import Spinner from "../spinner"
// import { NavLink } from "react-router-dom";
import AddItem from "../addItem"
import "./index.css"
class List extends React.Component {
    constructor() {
        super()
        this.state = {
            item: null,
            loading: true
        }
    }

    services = new Services()
    componentDidMount() {
        this.services
            .getAllItem()
            .then((item) => {
                this.setState({
                    item,
                    loading: false
                })
            })
    }

    render() {
        const { loading } = this.state
        return (
            <div className="items">
                {loading ?
                <Spinner/> :
                  <ul className="itemsList">
                  {this.state.item.map((items) => {
                      return (
                          <li key={items.id}>
                              <a href={`/items/${items.id}`} target="_self">
                              {items.id}  --- {  items.title}
                              </a> 
                          </li>)
                  })}
                 </ul> }
                 <AddItem />
            </div>
        )
    }
}

export default List