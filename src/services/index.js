import axios from 'axios'

export default class Services {

    base = axios.create({
        baseURL: 'https://jsonplaceholder.typicode.com/',
    });
    getAllItem  = async() => {
         const posts = await this.base.get(`/posts/`)
         return   posts.data.map(this._transformPost)
    }
    getItemId  = async (id) => {
        const post = await this.base.get(`/posts/${id}/`)
        return  this._transformPost(post.data)
    }
    deletePost = async (id) => {
        const post = await this.base.delete(`/posts/${id}/`)
        return  this._transformPost(post.data)
    }
    addItem = async (dataItem) => {
        const post = await this.base.post(`/posts/`, dataItem)
        return  this._transformPost(post.data)
    }
    _transformPost = (post) =>{
      return {
          id : post.id,
          title: post.title,
          body :post.body,
      }
    }
}