import React from 'react';
import List from "./components/list"
import ListItem from "./components/list-item"
import Header from "./components/header"
import Home from "./components/home"
import ErrorPage from "./components/errorpage"
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import "./App.css"
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/items/:id/" component={ListItem} />
          <Route path="/items" component={List} />
          <Route component={ErrorPage} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
